#ifndef PLANETERRIUM_ENGINESTATIC_H
#define PLANETERRIUM_ENGINESTATIC_H

#include <boost/random.hpp>
#include <memory>
#include <vector>

namespace utils {
namespace randomUtils {
typedef boost::ranlux4 ranlux4;
typedef boost::mt19937 mt19937;

/*!
 * @brief Creates a static instance of an engine.
 * @param p_engine Engine type
 * @param seed Seed for the random number generator
 */
void initializeEngine(std::shared_ptr<ranlux4> &p_engine, unsigned int seed);

/*!
 * @copydoc initializeEngine(std::shared_ptr<ranlux4>&, unsigned int)
 */
void initializeEngine(std::shared_ptr<mt19937> &p_engine, unsigned int seed);

/*!
 * @brief Returns the seed value that was used for initializing static engine of type Eng.
 * @warning If that engine was not seeded yet OR has been destroyed, than 0 is returned.
 * @tparam Eng
 * @return
 */
template<typename Eng>
unsigned int queryEngineSeed();

/*!
 * @brief Releases ownership of the static engine.
 *
 * If all other managed pointers to the static engine are also removed than the static engine is destroyed.
 * However, if another managed pointer is alive, than it keeps managing the static engine. Any new instance of
 * utils::randomUtils::Engine causes a new static engine to be allocated.
 */
template<typename Eng>
void eraseEngine();

/*!
 * @brief Identifies whether a static engine is currently assigned or not.
 * @return Returns a boolean vector specifying if each static engine is assigned or not to the engines tuple.
 */
std::vector<bool> assignedEngines();

/*!
 * @brief Index to an engine type within the static tuple of engine pointers
 * @tparam Eng Engine type
 * @return
 */
template<typename Eng>
int indexEng();

template<>
constexpr int indexEng<ranlux4>(){return 0;};

template<>
constexpr int indexEng<mt19937>(){return 1;};


}  // namespace randomUtils
}  // namespace utils
#endif //PLANETERRIUM_ENGINESTATIC_H
