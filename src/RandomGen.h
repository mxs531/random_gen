#ifndef PLANETERRIUM_RANDOMGEN_H
#define PLANETERRIUM_RANDOMGEN_H

#include <random>

#include "Distribution.h"
#include "Engine.h"

//! Utility routines
namespace utils {

/*!
 * @name Implemented instances of Engine
 */
//!@{
using randomUtils::ranlux4Eng;
using randomUtils::mt19937Eng;
//!@}

/*!
 * @name Implemented Distribution classes
 */
//!@{
using randomUtils::UniformDist;
using randomUtils::UniformIntDist;
using randomUtils::NormalDist;
using randomUtils::UniformSphere;
//!@}



/*!
 * @brief Random number generator. The engine and distribution must be one of the implemented types.
 *
 * Initialization of a random number engine can be time and memory intensive, if has to be done many times over.
 * This library manages static random number engines which are created when needed and destroyed when all objects
 * using them are gone.
 *
 * utils::randomUtils::Engine is the interface to the static random number engines. User only needs to specify
 * instances of a particular random number engine that have been implemented. They were brought to scope of utils
 * namespace.
 *
 * utils::randomUtils::Distribution is a base class for statistical distributions that can be sampled. User only needs
 * to handle specific instances of this class that were inherited into implemented distributions. They were brought
 * to scope of utils namespace.
 *
 *
 * @tparam Eng Instantiation of the Engine class
 * @tparam Dist Instantiation of Distribution class
 */
template<class Eng, class Dist>
class RandomGen {
public:
    /*!
     * @brief Initializes the random number generator with a particular distribution.
     * @param dist Random numbers are distributed according to this distribution. Can be specified using initialization
     * list e.g. RandomGen<E,D>({1.0, 2.0})
     * @param seed Seed to the random number engine
     */
    explicit RandomGen(Dist dist, unsigned int seed = std::random_device{}()) :
            m_engine(seed), m_dist(dist) {
    };

    ~RandomGen() = default;


    typedef typename Dist::result_type result_type; //!< type of generated random numbers

    /*!
     * @brief Generates a random number
     * @return Random number
     */
    result_type operator()() {return m_dist(m_engine);};

    typedef typename Eng::t_eng t_eng;
    typedef typename Dist::t_dist t_dist;

    Eng m_engine; //!< Random number engine
    Dist m_dist; //!< Random number distribution
}; // class RandomGen

} // namespace utils

#endif //PLANETERRIUM_RANDOMGEN_H
