#ifndef PLANETERRIUM_ENGINE_H
#define PLANETERRIUM_ENGINE_H

#include <boost/random.hpp>
#include <memory>
#include <random>

#include "Distribution.h"
#include "engineStatic.h"

namespace utils {

namespace randomUtils {

/*!
 * @brief A wrapper interface to the static random number generator engines. Designed for use with RandomGen.
 *
 * @warning All initializations use the same static engine of type Eng.
 *
 * @tparam Eng Type of random number engine with implemented interface
 */
template<class Eng>
class Engine {
public:
    template<class Dist, typename... Params> friend
    class Distribution;

    explicit Engine(unsigned int seed) {
        initialize(seed);
    };

    Engine(const Engine<Eng> &) { };

    Engine &operator=(const Engine<Eng> &) { };

    bool operator==(const Engine<Eng> &other) {return m_engine == other.m_engine;};

    bool operator!=(const Engine<Eng> &other) {return !(m_engine == other.m_engine);};

    ~Engine() = default;

    /*!
     * @copydoc utils::randomUtils::queryEngineSeed
     */
    unsigned int seed() {return queryEngineSeed<Eng>();};

    /*!
     * @brief Erases local pointer to the static engine
     */
    void eraseEngine() {m_engine.reset();};

    /*!
     * @copydoc utils::randomUtils::eraseEngine
     */
    static void eraseStaticEngine() {randomUtils::eraseEngine<Eng>();};

    /*!
     * @brief Checks if an engine is assigned
     * @return True if no engine is assigned, false otherwise
     */
    bool empty() const {return !m_engine;};

    typedef Eng t_eng; //! Type of the engine

protected:
    /*!
     * @brief Initializes shared management of a static engine.
     * @param seed Seed for the random number generator. Only used if the static engine was not initialized yet.
     */
    void initialize(unsigned int seed) {
        initializeEngine(m_engine, seed);
    }

    std::shared_ptr<Eng> m_engine; //! Managed pointer to a static engine
};

/*!@name Engines
 * Instantiations of the Engine class. The engines are static, so any different declarations use the same engine.
 */
//!@{
/*!
 * @brief Ranlux4 random number engine. Fast and has low memory overhead, but relatively short cycle.
 *
 * See boost documentation for more details.
 */
typedef Engine<ranlux4> ranlux4Eng;

/*!
 * @brief Rather slow, but very robust random number engine.
 *
 * See boost documentation for more details.
 */
typedef Engine<mt19937> mt19937Eng;
//!@}
}  // namespace randomUtils
}  // namespace utils

#endif //PLANETERRIUM_ENGINE_H
