#include "engineStatic.h"

#include <iostream>
#include <tuple>
#include <array>

namespace utils {
namespace randomUtils {

namespace {
/*!
* @brief Pointers to all implemented engines.
*
* The engines live here once initialized by a utils::randomUtils::Engine, so there is only ever one engine per
* engine type, even with multiple Engine objects of the same type.
* The engines remain alive until the end of the program or eraseEngines is called.
*/
std::tuple<std::shared_ptr<ranlux4>,
        std::shared_ptr<mt19937>> p_engines;
std::array<unsigned int, 2> seeds{0, 0};
}  // namespace

void initializeEngine(std::shared_ptr<ranlux4> &p_engine, unsigned int seed) {
    std::shared_ptr<ranlux4> &p_ranlux4Eng = std::get<indexEng<ranlux4>()>(p_engines);
    if (p_ranlux4Eng == nullptr) {
        p_ranlux4Eng = std::make_shared<ranlux4>(seed);
        p_engine = p_ranlux4Eng;
        seeds[indexEng<ranlux4>()] = seed;
    } else {
        p_engine = p_ranlux4Eng;
    }
}

template<>
unsigned int queryEngineSeed<ranlux4>() {
    return seeds[indexEng<ranlux4>()];
}

template<>
void eraseEngine<ranlux4>() {
    std::get<indexEng<ranlux4>()>(p_engines).reset();
    seeds[indexEng<ranlux4>()] = 0;
}

void initializeEngine(std::shared_ptr<mt19937> &p_engine, unsigned int seed) {
    std::shared_ptr<mt19937> &p_mt19937Eng = std::get<indexEng<mt19937>()>(p_engines);
    if (p_mt19937Eng == nullptr) {
        p_mt19937Eng = std::make_shared<mt19937>(seed);
        p_engine = p_mt19937Eng;
        seeds[indexEng<mt19937>()] = seed;
    } else {
        p_engine = p_mt19937Eng;
    }
}

template<>
unsigned int queryEngineSeed<mt19937>() {
    return seeds[indexEng<mt19937>()];
}

template<>
void eraseEngine<mt19937>() {
    std::get<indexEng<mt19937>()>(p_engines).reset();
    seeds[indexEng<mt19937>()] = 0;
}

std::vector<bool> assignedEngines() {
    return std::initializer_list<bool>{std::get<0>(p_engines) != nullptr, std::get<1>(p_engines) != nullptr};
};

}  // namespace randomUtils
}  // namespace utils
