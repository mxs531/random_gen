#ifndef PLANETERRIUM_DISTRIBUTION_H
#define PLANETERRIUM_DISTRIBUTION_H

#include <boost/random.hpp>
#include <tuple>
#include <memory>

namespace utils {
namespace randomUtils {

typedef boost::random::uniform_real_distribution<> uniform_real_distribution;
typedef boost::random::uniform_int_distribution<> uniform_int_distribution;
typedef boost::random::normal_distribution<> normal_distribution;
typedef boost::random::uniform_on_sphere<> uniform_on_sphere;

/*!
 * @brief General statistical distribution that can be sampled by a random number engine. Designed for utils::RandomGen
 */
template<class Dist, typename... Params>
class Distribution {
protected:
    explicit Distribution(Params... params, std::string name) :
            m_dist(params...), m_params(params...), m_name(std::move(name)) { };

    ~Distribution() = default;

public:
    typedef typename Dist::result_type result_type; //!< type for generated random numbers

    Distribution &operator=(const Distribution &other) {
        update(other.m_params);
    }

    explicit Distribution(const Dist &other) : m_dist(other.m_dist.param()), m_params(other->m_params),
                                               m_name(std::move(other->m_name)) { };

    /*!
     * @brief Uses the random number generator engine to sample the distribution
     * @tparam Engine random number generator engine
     * @param engine a particular Boost generator
     * @return random number from this distribution
     */
    template<typename Eng>
    result_type operator()(Eng &engine) {return m_dist(*engine.m_engine.get());};

    /*!
     * @brief Updates the parameters.
     *
     * @param params new parameters of the distribution
     */
    void update(Params... params) {
        auto new_params = typename Dist::param_type(params...);
        m_dist.param(new_params);
        m_params = std::make_tuple(params...);
    }

    /*!
     * @brief Returns a copy of parameters
     */
    std::tuple<Params...> params() const {return m_params;}

protected:
    Dist m_dist; //!< A random distribution, currently assumed to be one of Boost's
    std::tuple<Params...> m_params; //!< Parameters defining the distribution

public:
    std::string m_name; //!< Name of the distribution
    typedef Dist t_dist; //!< type of the distribution
};

/*!@name Distributions
 * Instantiations of the Distribution class.
 */
//!@{
/*!
 * @brief Uniform distribution of doubles
 *
 * Wrapper over boost::random::uniform_real_distribution. Sees boost for more details
 */
class UniformDist
        : public randomUtils::Distribution<uniform_real_distribution, double, double> {
public:
    UniformDist(double min, double max) : Distribution(min, max, "uniform distribution of doubles") { }

    ~UniformDist() = default;

    //! Lower bound of the distribution
    double min() const {return m_dist.min();};

    //! Upper bound of the distribution
    double max() const {return m_dist.max();};
};

/*!
 * @brief Uniform distribution of ints
 *
 * Wrapper over boost::random::uniform_int_distribution. Sees boost for more details
 */
class UniformIntDist : public randomUtils::Distribution<uniform_int_distribution, int, int> {
public:
    UniformIntDist(int min, int max) : Distribution(min, max, "uniform distribution of doubles") { }

    ~UniformIntDist() = default;

    //! Lower bound of the distribution
    int min() const {return m_dist.min();};

    //! Upper bound of the distribution
    int max() const {return m_dist.max();};
};

/*!
 * @brief Normal distribution
 *
 * Wrapper over boost::random::normal_distribution. Sees boost for more details
 */
class NormalDist : public randomUtils::Distribution<normal_distribution, double, double> {
public:
    NormalDist(double mean, double stdev) : Distribution(mean, stdev, "normal distribution") { }

    NormalDist(const NormalDist &other) : Distribution(other) { };

    ~NormalDist() = default;

    //! Mean value of the distribution
    double mean() const {return m_dist.mean();};

    //! Standard deviation of the distribution
    double sigma() const {return m_dist.sigma();};
};

/*!
 * @brief Uniform distribution on the surface of the sphere
 *
 * Wrapper over boost::random::uniform_on_sphere. Sees boost for more details
 */
class UniformSphere : public randomUtils::Distribution<uniform_on_sphere, int> {
public:
    //! @param dim Dimension of the sphere (1-sphere = ring, 2-sphere = sphere, 3-sphere = glome, etc)
    UniformSphere(int dim=3) : Distribution(dim, "uniform distribution on sphere") { }

    UniformSphere(const UniformSphere &other) : Distribution(other) { };

    ~UniformSphere() = default;
};
//!@}


}; // namespace randomUtils
} // namespace utils

#endif //PLANETERRIUM_DISTRIBUTION_H
