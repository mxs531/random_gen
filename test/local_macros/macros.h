#ifndef PLANETERRIUM_USEFUL_H
#define PLANETERRIUM_USEFUL_H

// Test fails if the program exits in any way other than with code 0
#define EXPECT_NO_EXIT(statement)EXPECT_EXIT({{statement} ::exit(0);}, ::testing::ExitedWithCode(0),"")\
<<"           Expected no exit"

// Test aborts if the program exits in any way other than with code 0
#define ASSERT_NO_EXIT(statement)ASSERT_EXIT({{statement} ::exit(0);}, ::testing::ExitedWithCode(0),"")\
<<"           Expected no exit"

#endif //PLANETERRIUM_USEFUL_H
