#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <type_traits>

#include <Distribution.h>
#include <macros.h>


// Tests specialized instances of the Distribution class

TEST(TestDistribution, constructors) {
    using namespace utils::randomUtils;
    EXPECT_NO_EXIT(UniformDist(1.0, 2.0););
    EXPECT_NO_EXIT(UniformDist(-1.0, 1.0););
    EXPECT_NO_EXIT(UniformDist(-2.0, -1.0););

    EXPECT_NO_EXIT(UniformIntDist(1, 2););
    EXPECT_NO_EXIT(UniformIntDist(-1, 1););
    EXPECT_NO_EXIT(UniformIntDist(-2, -1););

    EXPECT_NO_EXIT(NormalDist(0.0, 2.0););

    EXPECT_EXIT(NormalDist(0.0, -2.0), ::testing::KilledBySignal(SIGABRT), ".*");
}

TEST(TestDistribution, UniformDistribution) {
    using namespace utils::randomUtils;
    double a = -1.0, b = 1.0;
    UniformDist dist(a, b);
    EXPECT_NEAR(dist.min(), a, 1.0e-14);
    EXPECT_NEAR(dist.max(), b, 1.0e-14);
    bool same_type = std::is_same<uniform_real_distribution, UniformDist::t_dist>::value;
    EXPECT_TRUE(same_type);

    auto params = dist.params();
    EXPECT_NEAR(std::get<0>(params), a, 1.0e-14);
    EXPECT_NEAR(std::get<1>(params), b, 1.0e-14);

    double new_a = -2.0, new_b = 2.0;
    dist.update(new_a, new_b);
    params = dist.params();
    EXPECT_NEAR(std::get<0>(params), new_a, 1.0e-14);
    EXPECT_NEAR(std::get<1>(params), new_b, 1.0e-14);
}

TEST(TestDistribution, UniformIntDistribution) {
    using namespace utils::randomUtils;
    int a = -1, b = 1;
    UniformIntDist dist(a, b);
    EXPECT_EQ(dist.min(), a);
    EXPECT_EQ(dist.max(), b);
    bool same_type = std::is_same<uniform_int_distribution, UniformIntDist::t_dist>::value;
    EXPECT_TRUE(same_type);
}

TEST(TestDistribution, NormalDistribution) {
    using namespace utils::randomUtils;
    double mean = 0.0, stdev = 1.0;
    NormalDist dist(mean, stdev);
    EXPECT_NEAR(dist.mean(), mean, 1.0e-14);
    EXPECT_NEAR(dist.sigma(), stdev, 1.0e-14);
    bool same_type = std::is_same<normal_distribution, NormalDist::t_dist>::value;
    EXPECT_TRUE(same_type);
}

TEST(TestDistribution, UniformSphere){
    using namespace utils::randomUtils;
    UniformSphere dist();
    bool same_type = std::is_same<uniform_on_sphere, UniformSphere::t_dist>::value;
    EXPECT_TRUE(same_type);
    UniformSphere dist2(2);
}
