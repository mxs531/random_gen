#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <vector>

#include <RandomGen.h>

TEST(testRandomGen, initializer) {
    using namespace utils;
    ranlux4Eng::eraseStaticEngine(); // release static engine

    double mean = 1.0, sigma = 2.0;
    unsigned seed = 1;
    NormalDist dist(mean, sigma);
    RandomGen<ranlux4Eng, NormalDist> a(dist, seed);
    RandomGen<ranlux4Eng, NormalDist> b({mean, sigma}, seed);
    RandomGen<ranlux4Eng, NormalDist> c({mean, sigma}); // Seeded with default non-deterministic random number generator
    EXPECT_EQ(a.m_engine.seed(), seed);
    EXPECT_EQ(b.m_engine.seed(), seed);
    EXPECT_EQ(c.m_engine.seed(), seed); // Engine is static!

    a.m_engine.eraseEngine();
    b.m_engine.eraseEngine();
    c.m_engine.eraseEngine();
    ranlux4Eng::eraseStaticEngine();

    // All references to static engine should have been erased and the engine freed
    RandomGen<ranlux4Eng, NormalDist> d({mean, sigma}); // Seeded with default non-deterministic random number generator
    EXPECT_NE(d.m_engine.seed(), seed);

    ranlux4Eng::eraseStaticEngine(); // release static engine
}

TEST(testRandomGen, randomNumbers) {
    using namespace utils;
    using ::testing::Pointwise;
    using ::testing::DoubleNear;
    ranlux4Eng::eraseStaticEngine(); // release static engine

    int min = 1, max = 10;
    unsigned seed = 1;
    std::vector<int> ref{6, 3, 4, 1, 7};
    RandomGen<ranlux4Eng, UniformIntDist> a({min, max}, seed);
    std::vector<int> rndVal{a(), a(), a(), a(), a()};
    EXPECT_THAT(rndVal, ::testing::ContainerEq(ref));

    double mean = 1.0, sigma = 10.0;
    std::vector<double> refDbl{-15.647254947536, -2.619803395237, 0.24068693459806, -5.32571475225, 10.467532248892};
    RandomGen<ranlux4Eng, NormalDist> b({mean, sigma}, seed);
    std::vector<double> rndValDbl{b(), b(), b(), b(), b()};
    ASSERT_THAT(rndValDbl, Pointwise(DoubleNear(1.0e-12),refDbl));

    ranlux4Eng::eraseStaticEngine(); // release static engine
}

TEST(testRandomGen, UniformSphere) {
    using namespace utils;
    ranlux4Eng::eraseStaticEngine(); // release static engine

    unsigned seed = 1;
    RandomGen<ranlux4Eng, UniformSphere> a({3}, seed);
    std::vector<double> vec = a();
    double dot = 0.0;
    for (auto i: vec) dot += i*i;
    ASSERT_THAT(dot, ::testing::DoubleNear(1.0,1.0e-12));
    ASSERT_EQ(vec.size(), 3);
    RandomGen<ranlux4Eng, UniformSphere> b(UniformSphere(2));

    ranlux4Eng::eraseStaticEngine(); // release static engine
}
