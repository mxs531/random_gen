#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <type_traits>

#include <Engine.h>
#include <engineStatic.h>

// Testing implemented instances of the Engine class and setup of the static engines.

// This should be done through a templated fixture class and TEMPLATED_TEST

TEST(testEngine, testEngineranlux4){
    using namespace utils::randomUtils;
    typedef ranlux4Eng TypeParam;
    TypeParam::eraseStaticEngine(); // release static engine

    unsigned seed = 1;
    TypeParam eng1(seed);
    TypeParam eng2(seed+1);
    EXPECT_TRUE(eng1==eng2) << "Two instances of the same Engine should be equal";

    EXPECT_TRUE(eng1.seed() == seed);
    EXPECT_TRUE(eng1.seed() == eng2.seed());

    bool check = std::is_same<TypeParam::t_eng, ranlux4>::value;
    EXPECT_TRUE(check) << "check the stored engine type";

    auto a = assignedEngines();
    EXPECT_TRUE(a[indexEng<TypeParam::t_eng>()]);

    eng1.eraseEngine();
    eng2.eraseEngine();
    EXPECT_TRUE(eng1.empty());
    EXPECT_TRUE(eng2.empty());

    Engine<TypeParam::t_eng>::eraseStaticEngine();
    a = assignedEngines();
    EXPECT_FALSE(a[indexEng<TypeParam::t_eng>()]);
    TypeParam::eraseStaticEngine(); // release static engine
}

TEST(testEngine, testEnginemt19937){
    using namespace utils::randomUtils;
    typedef mt19937Eng TypeParam;
    TypeParam::eraseStaticEngine(); // release static engine

    unsigned seed = 1;
    TypeParam eng1(seed);
    TypeParam eng2(seed+1);
    EXPECT_TRUE(eng1==eng2) << "Two instances of the same Engine should be equal";

    EXPECT_TRUE(eng1.seed() == seed);
    EXPECT_TRUE(eng1.seed() == eng2.seed());

    bool check = std::is_same<TypeParam::t_eng, mt19937>::value;
    EXPECT_TRUE(check) << "check the stored engine type";

    auto a = assignedEngines();
    EXPECT_TRUE(a[indexEng<TypeParam::t_eng>()]);

    eng1.eraseEngine();
    eng2.eraseEngine();
    EXPECT_TRUE(eng1.empty());
    EXPECT_TRUE(eng2.empty());

    Engine<TypeParam::t_eng>::eraseStaticEngine();
    a = assignedEngines();
    EXPECT_FALSE(a[indexEng<TypeParam::t_eng>()]);
    TypeParam::eraseStaticEngine(); // release static engine
}

TEST(testEngine, testEngines){
    using namespace utils::randomUtils;
    ranlux4Eng::eraseStaticEngine(); // release static engine
    mt19937Eng::eraseStaticEngine(); // release static engine

    unsigned seed = 1;
    ranlux4Eng eng1(seed);
    mt19937Eng eng2(seed+1);
    auto a = assignedEngines();
    EXPECT_TRUE(a[indexEng<ranlux4Eng::t_eng>()]);
    EXPECT_TRUE(a[indexEng<mt19937Eng::t_eng>()]);

    eng1.eraseEngine();
    eng2.eraseEngine();
    a = assignedEngines();
    EXPECT_TRUE(a[indexEng<ranlux4Eng::t_eng>()]);
    EXPECT_TRUE(a[indexEng<mt19937Eng::t_eng>()]);

    ranlux4Eng::eraseStaticEngine();
    mt19937Eng::eraseStaticEngine();
    a = assignedEngines();
    EXPECT_FALSE(a[indexEng<ranlux4Eng::t_eng>()]);
    EXPECT_FALSE(a[indexEng<mt19937Eng::t_eng>()]);

    EXPECT_EQ(queryEngineSeed<ranlux4Eng::t_eng>(), 0);
    EXPECT_EQ(queryEngineSeed<mt19937Eng::t_eng>(), 0);

    ranlux4Eng::eraseStaticEngine(); // release static engine
    mt19937Eng::eraseStaticEngine(); // release static engine
}
